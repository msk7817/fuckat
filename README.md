# Fuckat Server

#### 介绍
模仿tomcat开发的一个服务器容器，简单易用，目前只支持windows平台

#### 软件架构
环境依赖：JDK1.8以上

架构：浏览器请求>fuckat>调用开发者的各种servlet>servlet进行各种业务处理>
调用fuckat的接口>返回数据给浏览器

#### 安装教程
1.安装JDK到你的电脑，并且把jdk的bin目录配置到你系统的path下；

2.通过git下载此项目

3.通过java开发相关工具，执行src下面的Boot.main函数启动项目

此项目为开源项目，欢迎你对该项目进行修改建议；

#### 应用服务开发说明
> 通过[fuckat-app-example](https://gitee.com/msk7817/fuckat-app-example)
仓库下载项目开发模板，基于此模板进行你的web项目开发，开发完成后，打成war包

web项目部署到fuckat的步骤：

开发环境：
1. 复制你的项目生成的war包到webapp目录下
2. 使用java开发工具或者命令行，启动src目录下的Boot.main函数
3. 根据config/server.xml配置的ip和端口，去浏览器访问你的web服务（servlet）
   比如：http://127.0.0.1:7878/example1
4. 您也可以直接双击bin下面的startup.bat启动fuckat容器

生产环境：
1. 下载fuckat-server-xxx.zip
2. 解压zip包到你的部署目录下
3. 把你要部署的war包放到解压后的webapp目录下
4. 双击bin/startup.bat启动服务
5. 您可以根据自己需求，修改config里面的服务地址和端口号

#### 参与贡献

sk


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
