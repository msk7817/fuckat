package com.sk.fuckat.servlet.parentServlet;

import com.sk.fuckat.entity.MyRequest;
import com.sk.fuckat.entity.MyResponse;

public interface MyServlet {
    void init() throws Exception;

    void service(MyRequest myRequest, MyResponse myResponse) throws Exception;

    void destroy();
}
