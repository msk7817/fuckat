package com.sk.fuckat.servlet.parentServlet;

import com.sk.fuckat.entity.MyRequest;
import com.sk.fuckat.entity.MyResponse;

import java.io.IOException;

public abstract class MyHttpServlet implements MyServlet{

    @Override
    public void service(MyRequest myRequest, MyResponse myResponse) throws Exception {
        if("GET".equalsIgnoreCase(myRequest.getMethod())){
            this.doGet(myRequest, myResponse);
        }else {
            this.doPost(myRequest, myResponse);
        }
    }

    public abstract void doGet(MyRequest myRequest, MyResponse myResponse) throws IOException;


    public abstract void doPost(MyRequest myRequest, MyResponse myResponse) throws IOException;
}
