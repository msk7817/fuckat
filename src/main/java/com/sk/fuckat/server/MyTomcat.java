package com.sk.fuckat.server;

import com.sk.fuckat.entity.MyRequest;
import com.sk.fuckat.entity.MyResponse;
import com.sk.fuckat.servlet.parentServlet.MyHttpServlet;
import com.sk.fuckat.utils.WarUtils;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.*;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class MyTomcat  {
    //private static int PORT = 9999;
    private static HashMap<String, Object> servletMapping = new HashMap<>(); //维护<servlet>标签信息
    private static HashMap<String, String> urlMapping = new HashMap<>(); //维护<servlet-mapping>标签信息
    private static HashMap<String, MyHttpServlet> servletMap = new HashMap<>(); //存储真正的Servlet类

    public static String ipAddr;
    public static int port;
    public static String protocol;
    public static String appBase;

    private MyTomcat() {
        try {
            this.initParams();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class LazyHolder {
        static final MyTomcat mytomcat = new MyTomcat();
    }

    private void initParams() throws Exception {
        String _path = System.getProperty("user.dir") + "\\config\\server.xml";
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read(_path);
        Element rootElement = document.getRootElement();
        List<Element> elements = rootElement.elements();
        elements.forEach((element) -> {
            if ("address".equalsIgnoreCase(element.getName())) {
                ipAddr = element.getText();
            }
            if("port".equalsIgnoreCase(element.getName())){
                port = Integer.parseInt(element.getText());
            }
            if("protocol".equalsIgnoreCase(element.getName())){
                protocol = element.getText();
            }
            if("appBase".equalsIgnoreCase(element.getName())){
                appBase = element.getText();
            }
        });
    }

    private void init() throws Exception {
        File sysConfig=new File(System.getProperty("user.dir") + "\\config");
        scanFiles(sysConfig);
        File appBaseDir = new File(System.getProperty("user.dir") + "\\"+appBase);
        scanWars(appBaseDir);
        scanFiles(appBaseDir);
    }

    public  void scanWars(File dirOrFile) throws Exception {
        if(dirOrFile!=null){
            if(dirOrFile.isDirectory()){
                File[] files=dirOrFile.listFiles();
                for (File flies2:files) {
                    scanWars(flies2);
                }
            }else{
                if(dirOrFile.getAbsolutePath().endsWith(".war")){
                    ucompressWar(dirOrFile);
                }
            }
        }else{
        }
    }

    public  void scanFiles(File file) throws Exception {
        if(file!=null){
            if(file.isDirectory()){
                File[] files=file.listFiles();//注意:这里只能用listFiles()，不能使用list()
                for (File flies2:files) {
                    scanFiles(flies2);
                }
            }else{
                if(file.getAbsolutePath().endsWith(".xml")){
                    readWebXml(file);
                }
            }
        }else{
        }
    }

    public void ucompressWar(File warFile){
        WarUtils.unzip( warFile, warFile.getParent()+"\\"+warFile.getName().split("-")[0]);
    }

    public  void readWebXml(File ffile) throws Exception {
        String WEBXMLPath = ffile.getAbsolutePath();
        String parentPath = ffile.getParent();
        SAXReader saxReader = new SAXReader();
        // saxReader.setFeature("http://cobertura.sourceforge.net" + "xml/coverage-04.dtd", false);
        Document document = saxReader.read(WEBXMLPath); //扫描web.xml
        Element rootElement = document.getRootElement(); //获取除<web-app外>所有根标签
        List<Element> elements = rootElement.elements(); //获取所有根标签中的集合标签
        elements.forEach((element) -> {
            if ("servlet".equalsIgnoreCase(element.getName())) {
                Element servletName = element.element("servlet-name");
                Element servletClass = element.element("servlet-class");
                log.info("servletName:" + servletName.getText());
                log.info("servletClass:" + servletClass.getText());
                if(ffile.getName().equals("default.xml")){
                    servletMapping.put(servletName.getText(), servletClass.getText());
                }else{
                    try {
                        Class serClasss = getClassByPath(parentPath+"\\classes", servletClass.getText());
                        servletMapping.put(servletName.getText(), serClasss);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if ("servlet-mapping".equalsIgnoreCase(element.getName())) {
                Element servletName = element.element("servlet-name");
                Element urlpattern = element.element("url-pattern");
                log.info("servletName:" + servletName.getText());
                log.info("servletClass:" + urlpattern.getText());
                urlMapping.put(urlpattern.getText(), servletName.getText());
            }
        });
        log.info(urlMapping.toString());
    }

    public Class getClassByPath(String parentPath, String fileName) throws Exception {
        File file = new File(parentPath);
        URL url = file.toURI().toURL();
        URLClassLoader classLoader = new URLClassLoader(new URL[]{url});
        Class aClass = classLoader.loadClass(fileName);
        return aClass;
    }


    public void start() {
        ServerSocket serverSocket = null;
        try {
            init();
            InetAddress inetAddress = InetAddress.getByName(ipAddr);
            serverSocket = new ServerSocket(port, 10, inetAddress);
            log.info("<<<<<--------------server started at:"+ipAddr+":"+port+"-------------->>>>>");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            while (!serverSocket.isClosed()) {
                log.info("waitting for http requests....");
                Socket server = serverSocket.accept();
                log.info("accepted a request:"+server.getRemoteSocketAddress().toString());
                ExecutorService tomcatPool = Executors.newFixedThreadPool(10); //Tomcat线程池
                tomcatPool.submit(new HttpAcceptThread(server));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    public class HttpAcceptThread implements Runnable {
        private Socket socket;

        public HttpAcceptThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            BufferedReader reader = null;
            OutputStream outputStream = null;
            String msg = null;
            String header = null;
            try {
                log.info("http处理进程开始--------------");
                MyRequest myRequest = new MyRequest(socket.getInputStream()); //解析http请求，MyRequest包装请求报文
                MyResponse myResponse = new MyResponse(socket.getOutputStream()); //MyResponse包装响应报文
                log.info("request请求封装：" + myRequest);
                MyHttpServlet handler = getHandlerByRequest(myRequest, myResponse); //根据请求类获取Servlet处理类
                if(handler != null){
                    handler.service(myRequest, myResponse); //调用Servlet处理类方法
                }
                log.info("http处理进程结束------------------");
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    outputStream.close();
                    if(!socket.isClosed()){
                        socket.getInputStream().close();
                        socket.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private MyHttpServlet getHandlerByRequest(MyRequest request, MyResponse response) throws IOException {
            MyHttpServlet handler = null;
            String uri = request.getUri();
            String servletName;
            if(uri.indexOf(".html")>0){
                servletName = "default";
            }else{
                servletName = urlMapping.get(uri); //根据uri获取servlet标签名称
            }
            if (servletName != null && !servletMap.containsKey(servletName)) { //懒加载：调用Servlet时才初始化
                String servlet = servletMapping.get(servletName).toString(); //根据servlet标签名称获取servlet全类名路径
                try {
                    if(servletName.equals("default")){
                        handler = (MyHttpServlet) Class.forName(servlet).newInstance();
                    }else{
                        Class c = (Class) servletMapping.get(servletName);
                        handler = (MyHttpServlet) c.newInstance();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    HashMap h = new HashMap();
                    h.put("content","<html><body><p>500 System Error</p></body></html>");
                    response.write(500, "System Error", "Content-type:text/html;charset=utf8", h);
                }
                if(handler != null){
                    servletMap.put(servletName, handler);
                }
            } else { //Servlet初始化后直接调用
                handler = servletMap.get(servletName);
                if (handler == null) {
                    HashMap h = new HashMap();
                    h.put("content","<html><body><p>404 Not Found</p></body></html>");
                    response.write(404, "Not Found", "Content-type:text/html;charset=utf8", h);
                }
            }
            return handler;
        }
    }

    public static MyTomcat getTomcat() {
        return LazyHolder.mytomcat;
    }
}
