package com.sk.fuckat.entity;

import lombok.Data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

@Data
public class MyRequest {
    private String uri;
    private String method;
    private HashMap<String, Object> reqArgs = new HashMap<>();

    public MyRequest(InputStream inputStream) throws IOException {
        BufferedReader reader = null;
        reader = new BufferedReader(new InputStreamReader(inputStream));
        String[] data = reader.readLine().split(" ");
        method = data[0];
        uri = data[1].lastIndexOf("?")>-1 ? data[1].substring(0, data[1].lastIndexOf("?")): data[1];
        if(data[1].lastIndexOf("?")>-1){
            String[] args = data[1].substring(data[1].lastIndexOf("?") + 1).split("&"); //截取请求参数
            for(String arg: args){
                String[] value = arg.split("=");
                reqArgs.put(value[0], value[1]);
            }
        }
    }

    public Object getAttribute(String key){
        return reqArgs.get(key);
    }
}
