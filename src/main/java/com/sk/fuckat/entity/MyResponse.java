package com.sk.fuckat.entity;

import com.sk.fuckat.server.MyTomcat;
import com.google.gson.Gson;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Date;
import java.util.HashMap;

@Data
@Slf4j
public class MyResponse {
    private OutputStream outputStream;
    private String http = MyTomcat.protocol;
    private int status;
    private String desc;
    private String header;
    private Date date;
    private HashMap<String, Object> body;

    public MyResponse(OutputStream outputStream){
        this.outputStream = outputStream;
    }

    public void write(int status, String desc, String contentType, HashMap body) {
        Gson gson = new Gson();
        String bodyJson;
        StringBuilder responseHeader = new StringBuilder();
        if(status == 200){
            bodyJson =  gson.toJson(body);
        }else{
            bodyJson = (String) body.get("content");
        }
        responseHeader.append(http+" ").append(status+" ").append(desc+"\r\n");
        responseHeader.append("Server:sk server"+"\r\n");
        responseHeader.append("Date:").append(new Date()).append("\r\n");
        responseHeader.append(contentType).append("\r\n");
        try {
            String ctt = URLDecoder.decode(bodyJson,"utf8");
            responseHeader.append("Content-Length:"+ctt.length()).append("\r\n");
            responseHeader.append("\r\n");
            responseHeader.append(ctt);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            outputStream.write(responseHeader.toString().getBytes("UTF-8"));
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendStaticResource(String filePath) throws IOException{
        byte[] buffer = new byte[2048];
        FileInputStream fis = null;
        File file = new File(filePath);
        String returnMsg = null;
        try {
            if(file.exists() && !file.isDirectory()){
                fis = new FileInputStream(file);
                StringBuilder sb = new StringBuilder();
                int length;
                while ((length = fis.read(buffer,0,buffer.length)) != -1){
                    sb.append(new String(buffer,0,length,"utf8"));
                }
                returnMsg = "HTTP/1.1 200 OK\r\n" +
                        "Content-Type: text/html;charset=utf8 \r\n" +
                        "Content-Length: "+sb.length()+"\r\n"+
                        "\r\n"+
                        sb.toString();
            }else{
                String errorMsg = "<h1>" + file.getName() + " file or directory not exists</h1>";
                returnMsg = "HTTP/1.1 404 File Not Fount\r\n" +
                        "Content-Type: text/html;charset=utf8 \r\n" +
                        "Content-Length: "+errorMsg.length()+"\r\n"+
                        "\r\n"+
                        errorMsg;
            }
            outputStream.write(returnMsg.getBytes("UTF-8"));
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fis != null){
                fis.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }


}
